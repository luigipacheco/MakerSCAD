/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

var mm_repository = {
  hash: function(data){
    var ret;
    if(typeof data == "string"){
      var len = data.length;
      ret = len.toString(16)+'_'+SparkMD5.hash(data);
    }
    else{ // Note that when data is ArrayBuffer -> typeof data == "object"
      len = data.byteLength;
      ret = len.toString(16)+'_'+SparkMD5.ArrayBuffer.hash(data);
    }
    return ret;
  },
  // arrayBufferHash: function(data){
  //   len = data.byteLength;
  //   return len.toString(16)+'_'+SparkMD5.ArrayBuffer.hash(data);
  // },
  getName: function(repository, item){
    try{
      var name = repository[item].name;
    }
    catch(err){
      var name = "";
    }
    return name;
  },
  getMetadata: function(repository){
    var metadata = {};
    Object.keys(repository).forEach(function(key){
      metadata[key] = {
        name: repository[key].name,
        // TODO: Change name and ext to attr
        // ext: repository[key].ext,
        url: repository[key].url
      }
    });
    return metadata;
  },
  // This should be installed in a AssetsBehaviors
  // Utility functions:
  _setAssetDraw: function(that, repository, id, data, name, url) {
    if(typeof repository[id] == "undefined"){
      that.push("assets", id); // This is the way to add values to an array property on polymer
    }
    repository[id] = {data: that._newObject(data),
      name: name,
      url: url};
    that.selected = id;
    that._drawAsset(repository[id]);
  },
  _setAsset:  function(repository, id, data, name, url) {
    repository[id] = {data: data,
      name: name,
      url: url};
  },
  _nameParts: function(name){
    var parts = name.split(".");
    var len = parts.length;
    var ext = "";
    var name_no_ext = name;
    if(len>1){
      ext = parts[len-1].toLowerCase();
      name_no_ext = name.slice(0,name.lastIndexOf("."));
    }
    return {ext: ext, name: name, name_no_ext: name_no_ext};
  },
  _urlParts: function(url){
    var parts1 = url.split("?");
    //var parts2 = parts1[0].split(".");
    var urlParts = parts1[0].split("/");
    var name = urlParts[urlParts.length-1];
    //var name_no_ext = name1;
    //var len = parts2.length;
    //var ext = "";
    //if(len>1){
    //  ext = parts2[len-1].toLowerCase();
    //  name_no_ext = name.slice(0,name.lastIndexOf("."));
    //}
    var nameParts = this._nameParts(name);
    return {ext: nameParts.ext, name: name, name_no_ext: nameParts.name_no_ext};
  },
  // Name utility functions
  buildItemNameID: function(name, data, url){
    return {
      hash: this.hash(data),
      name: name,
      data: data,
      url: url
    }
  },
  setAssetNameID: function(repository, name, data, url, duplicates){
    // If the same name and hash exist do nothing
    // If the same name and different hash exist return new name
    var i=0;
    var item = this.buildItemNameID(name, data, url);
    var name1 = url;
    if(url == ""){
      var nameParts = this._nameParts(name);
      name1 = name;
      while(repository[name1] != undefined) {
        if(!duplicates){
          if(item.hash == repository[name1].hash){
            break;
          }
        }
        i += 1;
        name1 = nameParts.name_no_ext+"_"+i+"."+nameParts.ext;
      }
      item.name = name1;
    }
    else {
      var urlParts = this._urlParts(url);
      item.name = urlParts.name;
    }
    repository[name1] = item;
    return name1;
  },
  updateAssetNameID: function(repository, name, newData){
    var item = repository[name];
    var item1 = this.buildItemNameID(name, newData, item.url);
    repository[name] = item1;
  }
}

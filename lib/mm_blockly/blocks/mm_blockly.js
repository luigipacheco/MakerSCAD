/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

'use strict';

goog.provide('Blockly.Blocks.mm');

goog.require('Blockly.Blocks');
goog.require('Blockly.EditorMutator');

Blockly.Blocks['mm_text_append'] = {
  init: function() {
    this.appendValueInput("TEXT")
        .appendField("to")
        .appendField(new Blockly.FieldTextInput("var1"), "variable")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("append text");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(false);
    this.setColour(Blockly.Blocks.texts.HUE);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/');
  }
};

Blockly.Blocks['mm_xyz'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("x")
        .appendField(new Blockly.FieldTextInput("0"), "X");
    this.appendDummyInput()
        .appendField("y")
        .appendField(new Blockly.FieldTextInput("0"), "Y");
    this.appendDummyInput()
        .appendField("z")
        .appendField(new Blockly.FieldTextInput("0"), "Z");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_list_xyz'] = {
  init: function() {
    this.appendValueInput("X")
        .setCheck("Number")
        .appendField("x");
    this.appendValueInput("Y")
        .setCheck("Number")
        .appendField("y");
    this.appendValueInput("Z")
        .setCheck("Number")
        .appendField("z");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_code_part'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("JS code")
        .appendField(new Blockly.FieldTextInput(""), "code");
    this.setOutput(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_code_line'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("JS code line")
        .appendField(new Blockly.FieldTextInput(""), "code");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_access_field'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("set")
        .appendField(new Blockly.FieldVariable("item"), "variable");
    this.appendDummyInput()
        .appendField(".")
        .appendField(new Blockly.FieldTextInput(""), "field");
    this.appendValueInput("input")
        .appendField("to");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(true);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_set_to'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("set")
        .appendField(new Blockly.FieldTextInput(""), "code");
    this.appendValueInput("input")
        .appendField("to");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(true);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_call'] = {
  /**
   * Block for creating a list with any number of elements of any type.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.LISTS_CREATE_WITH_HELPURL);
    this.setColour(Blockly.Blocks.lists.HUE);
    this.appendAddSubNamed("", 'items',
                           null,
                           "");
    this.itemCount_ = 1;
    this.updateShape_();
    this.setOutput(true, 'Array');
    this.setTooltip(Blockly.Msg.LISTS_CREATE_WITH_TOOLTIP);
  }
};

Blockly.Blocks['mm_function_call'] = {
  /**
   * Block for creating a list with any number of elements of any type.
   * @this Blockly.Block
   */
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput("function_call1"), "NAME")
        .appendField("()");
    // this.appendValueInput("NAME")
    //  .setCheck("String")
    //  .appendField("function name:");
    // this.appendDummyInput()
    //  .appendField("____");
    //this.setInputsInline(false);
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    if (this.workspace.options.useMutators) {
      this.setMutator(new Blockly.Mutator(['lists_create_with_item']));
    } else {
      this.appendAddSubGroup("         args", 'items',
                             null,
                             "         args");
      // this.appendAddSubGroup(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH, 'items',
      //                      null,
      //                      Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
      // this.appendAddSubStatement(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH, 'items',
      //                      null,
      //                      Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
    }
    this.appendStatementInput("chain")
        .setCheck(["Method", "Field"]);
    this.itemCount_ = 1;
    this.updateShape_();
    this.setPreviousStatement(true, "Method");
    //this.setOutput(true, 'Array');
    this.setTooltip('');
  },
  /**
   * Create XML to represent list inputs.
   * @return {!Element} XML storage element.
   * @this Blockly.Block
   */
  mutationToDom: function() {
    var container = document.createElement('mutation');
    container.setAttribute('items', this.itemCount_);
    return container;
  },
  /**
   * Parse XML to restore the list inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this Blockly.Block
   */
  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
    this.updateShape_();
  },
  /**
   * Populate the mutator's dialog with this block's components.
   * @param {!Blockly.Workspace} workspace Mutator's workspace.
   * @return {!Blockly.Block} Root block in mutator.
   * @this Blockly.Block
   */
  decompose: function(workspace) {
    var containerBlock = workspace.newBlock('lists_create_with_container');
    containerBlock.initSvg();
    var connection = containerBlock.getInput('STACK').connection;
    for (var i = 0; i < this.itemCount_; i++) {
      var itemBlock = workspace.newBlock('lists_create_with_item');
      itemBlock.initSvg();
      connection.connect(itemBlock.previousConnection);
      connection = itemBlock.nextConnection;
    }
    return containerBlock;
  },
  /**
   * Reconfigure this block based on the mutator dialog's components.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  compose: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    // Count number of inputs.
    var connections = [];
    while (itemBlock) {
      connections.push(itemBlock.valueConnection_);
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
    // Disconnect any children that don't belong.
    for (var i = 0; i < this.itemCount_; i++) {
      var connection = this.getInput('ADD' + i).connection.targetConnection;
      if (connection && connections.indexOf(connection) == -1) {
        connection.disconnect();
      }
    }
    this.itemCount_ = connections.length;
    this.updateShape_();
    // Reconnect any child blocks.
    for (var i = 0; i < this.itemCount_; i++) {
      Blockly.Mutator.reconnect(connections[i], this, 'ADD' + i);
    }
  },
  /**
   * Store pointers to any connected child blocks.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  saveConnections: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    var i = 0;
    while (itemBlock) {
      var input = this.getInput('ADD' + i);
      itemBlock.valueConnection_ = input && input.connection.targetConnection;
      i++;
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
  },
  /**
   * Modify this block to have the correct number of inputs.
   * @private
   * @this Blockly.Block
   */
  updateShape_: function() {
    if (this.itemCount_ && this.getInput('EMPTY')) {
      this.removeInput('EMPTY');
    } else if (!this.itemCount_ && !this.getInput('EMPTY')) {
      this.appendDummyInput('EMPTY')
          .appendField(Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
    }
    // Add new inputs.
    for (var i = 0; i < this.itemCount_; i++) {
      if (!this.getInput('ADD' + i)) {
        var input = this.appendValueInput('ADD' + i);
        if (i == 0) {
          input.appendField(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH);
        }
      }
    }
    // Remove deleted inputs.
    while (this.getInput('ADD' + i)) {
      this.removeInput('ADD' + i);
      i++;
    }
  }
};

Blockly.Blocks['mm_function_call_return'] = {
  /**
   * Block for creating a list with any number of elements of any type.
   * @this Blockly.Block
   */
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput("function_call1"), "NAME")
        .appendField("()");
    // this.appendValueInput("NAME")
    //  .setCheck("String")
    //  .appendField("function name:");
    // this.appendDummyInput()
    //  .appendField("____");
    //this.setInputsInline(false);
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    if (this.workspace.options.useMutators) {
      this.setMutator(new Blockly.Mutator(['lists_create_with_item']));
    } else {
      this.appendAddSubGroup("         args", 'items',
                             null,
                             "         args");
      // this.appendAddSubGroup(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH, 'items',
      //                      null,
      //                      Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
      // this.appendAddSubStatement(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH, 'items',
      //                      null,
      //                      Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
    }
    this.appendStatementInput("chain")
        .setCheck(["Method", "Field"]);
    this.itemCount_ = 1;
    this.updateShape_();
    //this.setPreviousStatement(true, "Method");
    this.setOutput(true, null); // , "Array"
    this.setTooltip('');
  },
  /**
   * Create XML to represent list inputs.
   * @return {!Element} XML storage element.
   * @this Blockly.Block
   */
  mutationToDom: function() {
    var container = document.createElement('mutation');
    container.setAttribute('items', this.itemCount_);
    return container;
  },
  /**
   * Parse XML to restore the list inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this Blockly.Block
   */
  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
    this.updateShape_();
  },
  /**
   * Populate the mutator's dialog with this block's components.
   * @param {!Blockly.Workspace} workspace Mutator's workspace.
   * @return {!Blockly.Block} Root block in mutator.
   * @this Blockly.Block
   */
  decompose: function(workspace) {
    var containerBlock = workspace.newBlock('lists_create_with_container');
    containerBlock.initSvg();
    var connection = containerBlock.getInput('STACK').connection;
    for (var i = 0; i < this.itemCount_; i++) {
      var itemBlock = workspace.newBlock('lists_create_with_item');
      itemBlock.initSvg();
      connection.connect(itemBlock.previousConnection);
      connection = itemBlock.nextConnection;
    }
    return containerBlock;
  },
  /**
   * Reconfigure this block based on the mutator dialog's components.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  compose: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    // Count number of inputs.
    var connections = [];
    while (itemBlock) {
      connections.push(itemBlock.valueConnection_);
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
    // Disconnect any children that don't belong.
    for (var i = 0; i < this.itemCount_; i++) {
      var connection = this.getInput('ADD' + i).connection.targetConnection;
      if (connection && connections.indexOf(connection) == -1) {
        connection.disconnect();
      }
    }
    this.itemCount_ = connections.length;
    this.updateShape_();
    // Reconnect any child blocks.
    for (var i = 0; i < this.itemCount_; i++) {
      Blockly.Mutator.reconnect(connections[i], this, 'ADD' + i);
    }
  },
  /**
   * Store pointers to any connected child blocks.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  saveConnections: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    var i = 0;
    while (itemBlock) {
      var input = this.getInput('ADD' + i);
      itemBlock.valueConnection_ = input && input.connection.targetConnection;
      i++;
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
  },
  /**
   * Modify this block to have the correct number of inputs.
   * @private
   * @this Blockly.Block
   */
  updateShape_: function() {
    if (this.itemCount_ && this.getInput('EMPTY')) {
      this.removeInput('EMPTY');
    } else if (!this.itemCount_ && !this.getInput('EMPTY')) {
      this.appendDummyInput('EMPTY')
          .appendField(Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
    }
    // Add new inputs.
    for (var i = 0; i < this.itemCount_; i++) {
      if (!this.getInput('ADD' + i)) {
        var input = this.appendValueInput('ADD' + i);
        if (i == 0) {
          input.appendField(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH);
        }
      }
    }
    // Remove deleted inputs.
    while (this.getInput('ADD' + i)) {
      this.removeInput('ADD' + i);
      i++;
    }
  }
};

Blockly.Blocks['mm_function'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("function")
        .appendField(new Blockly.FieldTextInput("arg1, arg2, etc"), "args");
    this.appendStatementInput("chain")
        .setCheck(null);
    this.setOutput(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_return'] = {
  init: function() {
    this.appendValueInput('ret')
        .setCheck(null)
        .appendField('return')
    this.setPreviousStatement(true, null);
    //this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_var'] = {
  init: function() {
    this.appendValueInput("val")
        .setCheck(null)
        .appendField("var")
        .appendField(new Blockly.FieldTextInput("var1"), "var");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_var_return'] = {
  init: function() {
    this.appendDummyInput()
        .appendField('')
        .appendField(new Blockly.FieldTextInput('var1'), 'NAME');
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_new'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("new");
    this.appendStatementInput("chain")
        .setCheck(null);
    //this.setPreviousStatement(true, null);
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_field'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("")
        .appendField(new Blockly.FieldTextInput("field1"), "NAME");
    this.appendStatementInput("chain")
        .setCheck(["Field","Method"]);
    this.setPreviousStatement(true, "Field");
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_adaptor'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("");
    this.appendStatementInput("chain")
        .setCheck(null);
    //this.setPreviousStatement(true, null);
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_statement'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("");
    this.appendStatementInput("chain")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_spread'] = {
  init: function() {
    this.appendValueInput("arg_array")
        .setCheck(null)
        .appendField("...");
    this.setOutput(true, null);
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_script_text'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField("JS editor");
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput("script.js"), "name");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setMutator(new Blockly.EditorMutator([]));
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_print'] = {
  init: function() {
    this.appendValueInput("msg")
        .setCheck(null)
        .appendField("print");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true, null);
    this.setColour(150);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_println'] = {
  init: function() {
    this.appendValueInput("msg")
        .setCheck(null)
        .appendField("println");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true, null);
    this.setColour(150);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_load_library'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField("library");
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput("script.js"), "name");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setMutator(new Blockly.EditorMutator([]));
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['mm_mqtt_init'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("mqtt init")
        .appendField(new Blockly.FieldTextInput("mqtt1"), "name");
    this.appendDummyInput()
        .appendField("ws")
        .appendField(new Blockly.FieldTextInput("ws://localhost:9001"), "ws");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['mm_mqtt_on_connect'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("mqtt on connect")
        .appendField(new Blockly.FieldTextInput("mqtt1"), "name");
    this.appendStatementInput("chain")
        .setCheck(null)
        .appendField("f(mq1)");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['mm_mqtt_subscribe'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("mqtt subscribe")
        .appendField(new Blockly.FieldTextInput("mqtt1"), "name");
    this.appendDummyInput()
        .appendField("topic")
        .appendField(new Blockly.FieldTextInput("makerscad1"), "topic");
    this.appendStatementInput("chain")
        .setCheck(null)
        .appendField("f(topic, message)");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['mm_mqtt_publish'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("mqtt publish")
        .appendField(new Blockly.FieldTextInput("mqtt1"), "name");
    this.appendDummyInput()
        .appendField("topic")
        .appendField(new Blockly.FieldTextInput("makerscad1"), "topic");
    this.appendValueInput("msg")
        .setCheck(null)
        .appendField("message");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['mm_gui_init'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("gui init");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['mm_gui_message'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("gui message: mmp.ui.")
        .appendField(new Blockly.FieldTextInput("msg1"), "name");
//    this.appendDummyInput()
//        .appendField("value")
//        .appendField(new Blockly.FieldTextInput("some message"), "value");
    this.appendStatementInput("chain")
        .setCheck(null)
        .appendField("onFinishChange(val)");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['mm_gui_value'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("gui value: mmp.ui.")
        .appendField(new Blockly.FieldTextInput("val1"), "name");
//    this.appendDummyInput()
//        .appendField("value")
//        .appendField(new Blockly.FieldTextInput("50"), "value");
    this.appendDummyInput()
        .appendField("min")
        .appendField(new Blockly.FieldTextInput("0"), "min");
    this.appendDummyInput()
        .appendField("max")
        .appendField(new Blockly.FieldTextInput("100"), "max");
    this.appendDummyInput()
        .appendField("step")
        .appendField(new Blockly.FieldTextInput("10"), "step");
    this.appendStatementInput("chain")
        .setCheck(null)
        .appendField("onChange(val)");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['mm_gui_color'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("gui color")
        .appendField(new Blockly.FieldTextInput("color1"), "name");
    this.appendDummyInput()
        .appendField("color")
        .appendField(new Blockly.FieldTextInput("#ffae23"), "color");
    this.appendStatementInput("chain")
        .setCheck(null)
        .appendField("onFinishChange(color)");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    //this.setOutput(true);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

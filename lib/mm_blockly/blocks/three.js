/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

'use strict';

goog.provide('Blockly.Blocks.three');

goog.require('Blockly.Blocks');
goog.require('Blockly.PolygonMutator');
goog.require('Blockly.AssetObjectMutator');

Blockly.Blocks['three_init'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("New Scene: lights")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "lights")
        .appendField("physics")
        .appendField(new Blockly.FieldCheckbox("FALSE"), "physijs");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

//Blockly.Blocks['three_init'] = {
  //init: function() {
    //this.appendDummyInput()
        //.appendField("CAD init");
    //this.appendValueInput("mesh")
        //.setAlign(Blockly.ALIGN_RIGHT)
        //.appendField("mesh");
    //this.setPreviousStatement(true, null);
    //this.setNextStatement(true, null);
    //this.setColour(330);
    //this.setTooltip('');
    //this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  //}
//};

Blockly.Blocks['three_scene_add'] = {
  init: function() {
    this.appendValueInput("mesh")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Add");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
  }
};

 Blockly.Blocks['three_new_scene'] = {
   init: function() {
     this.appendDummyInput()
         .appendField("new mm_scene no lights");
     this.setPreviousStatement(true, null);
     this.setNextStatement(true, null);
     this.setInputsInline(false);
     this.setColour(330);
     this.setTooltip('');
     this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
   }
 };

 Blockly.Blocks['three_new_basic_scene'] = {
   init: function() {
     this.appendDummyInput()
         .appendField("new mm_scene with lights");
     this.setPreviousStatement(true, null);
     this.setNextStatement(true, null);
     this.setInputsInline(false);
     this.setColour(330);
     this.setTooltip('');
     this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
   }
 };

 Blockly.Blocks['physi_new_basic_scene'] = {
   init: function() {
     this.appendDummyInput()
         .appendField("new basic physi mm_scene");
     this.setPreviousStatement(true, null);
     this.setNextStatement(true, null);
     this.setInputsInline(false);
     this.setColour(330);
     this.setTooltip('');
     this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
   }
 };

Blockly.Blocks['three_new_group'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Create group");
    this.setOutput(true);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
  }
};

Blockly.Blocks['three_group'] = {
  /**
   * Block for creating a list with any number of elements of any type.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.LISTS_CREATE_WITH_HELPURL);
    this.setColour(Blockly.Blocks.lists.HUE);
    this.appendAddSubGroup('New group', 'items',
                           null,
                           'G');
    this.itemCount_ = 2;
    this.updateShape_();
    //    this.appendValueInput('chain')
    //        .setCheck(null);
    //this.setInputsInline(true);
    this.setOutput(true, null);
//    this.setPreviousStatement(true, null);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/index.html');
  }
};

Blockly.Blocks['three_object_add'] = {
  init: function() {
    this.appendValueInput("mesh")
        .appendField(new Blockly.FieldTextInput("group"), "variable")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(".add");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
  }
};

Blockly.Blocks['three_animation'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Animation");
        //.appendField(new Blockly.FieldVariable("delta"), "delta");
    this.appendStatementInput("st")
        .setCheck(null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/animation.html');
  }
};

Blockly.Blocks['three_play_init'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("At play Start");
        //.appendField(new Blockly.FieldVariable("delta"), "delta");
    this.appendStatementInput("st")
        .setCheck(null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/animation.html');
  }
};

Blockly.Blocks['three_play_close'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("At play Close");
        //.appendField(new Blockly.FieldVariable("delta"), "delta");
    this.appendStatementInput("st")
        .setCheck(null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/animation.html');
  }
};

Blockly.Blocks['three_draw'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Draw");
        //.appendField(new Blockly.FieldVariable("delta"), "delta");
    this.appendStatementInput("st")
        .setCheck(null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['three_render'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Render");
    // TODO: JCOA Add field to set new camera.
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['three_light_ambient'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/ambientlight.png", 40, 40, "polygon"))
        .appendField("Ambient Light");
    this.appendValueInput("color")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("color");
    this.appendValueInput("intensity")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("intensity");
    this.setOutput(true);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_light_directional'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage("images/dlight.png", 40, 40, "text"))
        .appendField("Directional Light");
    this.appendValueInput("color")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("color");
    this.appendValueInput("intensity")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("intensity");
    this.setOutput(true);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_light_hemisphere'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/hemilight.png", 40, 40, "polygon"))
        .appendField("Hemisphere Light");
    this.appendValueInput("sky_color")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("sky color");
    this.appendValueInput("ground_color")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("ground color");
    this.appendValueInput("intensity")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("intensity");
    this.setOutput(true);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_light_point'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/pointlight.png", 40, 40, "polygon"))
        .appendField("Point Light");
    this.appendValueInput("color")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("color");
    this.appendValueInput("intensity")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("intensity");
    this.appendValueInput("distance")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("distance");
    this.appendValueInput("decay")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("decay");
    // this.appendValueInput("enableshadow")
    //     .setAlign(Blockly.ALIGN_RIGHT)
    //     .appendField("enableshadow");
    this.setOutput(true);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_light_spot'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/pointlight.png", 40, 40, "polygon"))
        .appendField("Spot Light");
    this.appendValueInput("color")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("color");
    this.appendValueInput("intensity")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("intensity");
    this.appendValueInput("distance")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("distance");
    this.appendValueInput("angle")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("angle");
    this.appendValueInput("penumbra")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("penumbra");
    this.appendValueInput("decay")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("decay");
    this.setOutput(true);
    this.setInputsInline(false);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_add_mesh_list'] = {
  init: function() {
    this.appendValueInput("mesh")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Add Mesh List");
    this.appendDummyInput()
        .appendField("____");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(false);
    this.setColour(330);
    this.itemCount_ = 1;
    this.updateShape_();
    this.setMutator(new Blockly.Mutator(['lists_create_with_item']));
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  },
    /**
   * Create XML to represent list inputs.
   * @return {!Element} XML storage element.
   * @this Blockly.Block
   */
  mutationToDom: function() {
    var container = document.createElement('mutation');
    container.setAttribute('items', this.itemCount_);
    return container;
  },
  /**
   * Parse XML to restore the list inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this Blockly.Block
   */
  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
    this.updateShape_();
  },
  /**
   * Populate the mutator's dialog with this block's components.
   * @param {!Blockly.Workspace} workspace Mutator's workspace.
   * @return {!Blockly.Block} Root block in mutator.
   * @this Blockly.Block
   */
  decompose: function(workspace) {
    var containerBlock = workspace.newBlock('lists_create_with_container');
    containerBlock.initSvg();
    var connection = containerBlock.getInput('STACK').connection;
    for (var i = 0; i < this.itemCount_; i++) {
      var itemBlock = workspace.newBlock('lists_create_with_item');
      itemBlock.initSvg();
      connection.connect(itemBlock.previousConnection);
      connection = itemBlock.nextConnection;
    }
    return containerBlock;
  },
  /**
   * Reconfigure this block based on the mutator dialog's components.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  compose: function(containerBlock) {
    var connections = [];
    while (itemBlock) {
      connections.push(itemBlock.valueConnection_);
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
    // Disconnect any children that don't belong.
    for (var i = 0; i < this.itemCount_; i++) {
      var connection = this.getInput('ADD' + i).connection.targetConnection;
      if (connection && connections.indexOf(connection) == -1) {
        connection.disconnect();
      }
    }
    this.itemCount_ = connections.length;
    this.updateShape_();
    // Reconnect any child blocks.
    for (var i = 0; i < this.itemCount_; i++) {
      Blockly.Mutator.reconnect(connections[i], this, 'ADD' + i);
    }
  },
  /**
   * Store pointers to any connected child blocks.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  saveConnections: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    var i = 0;
    while (itemBlock) {
      var input = this.getInput('ADD' + i);
      itemBlock.valueConnection_ = input && input.connection.targetConnection;
      i++;
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
  },
  /**
   * Modify this block to have the correct number of inputs.
   * @private
   * @this Blockly.Block
   */
  updateShape_: function() {
    if (this.itemCount_ && this.getInput('EMPTY')) {
      this.removeInput('EMPTY');
    } else if (!this.itemCount_ && !this.getInput('EMPTY')) {
      this.appendDummyInput('EMPTY')
          .appendField(Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
    }
    // Add new inputs.
    for (var i = 0; i < this.itemCount_; i++) {
      if (!this.getInput('ADD' + i)) {
        var input = this.appendValueInput('ADD' + i);
        if (i == 0) {
          input.appendField(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH);
        }
      }
    }
    // Remove deleted inputs.
    while (this.getInput('ADD' + i)) {
      this.removeInput('ADD' + i);
      i++;
    }
  }
};

Blockly.Blocks['three_part'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(new Blockly.FieldImage("images/part.png", 40, 40, "part"))
        .appendField("Boolean");
    this.appendValueInput("block1");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["union", "union"], ["subtract", "subtract"], ["intersect", "intersect"]]), "boolean_op");
    this.appendValueInput("block2"); // This breaks when using https://github.com/toebes-extreme/blockly.git it is a bug of that fork of blockly.
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-part-block');
  }
};

Blockly.Blocks['three_box'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/cube.png", 40, 40, "cube"))
        .appendField("Box");
    this.appendValueInput("size")
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck("Array")
        .appendField("size xyz");
    this.appendValueInput("segments")
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck("Number")
        .appendField("segments");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-box');
  }
};

Blockly.Blocks['physi_box'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/cube.png", 40, 40, "cube"))
        .appendField("physi box");
    this.appendValueInput("size")
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck("Array")
        .appendField("size xyz");
    this.appendValueInput("segments")
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck("Number")
        .appendField("segments");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-box');
  }
};

Blockly.Blocks['three_sphere'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/sphere.png", 40, 40, "sphere"))
        .appendField("Sphere");
    this.appendValueInput("diameter")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("diameter");
    this.appendValueInput("resolution")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("resolution");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-sphere');
  }
};

Blockly.Blocks['physi_sphere'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/sphere.png", 40, 40, "sphere"))
        .appendField("physi sphere");
    this.appendValueInput("diameter")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("diameter");
    this.appendValueInput("resolution")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("resolution");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-sphere');
  }
};

Blockly.Blocks['three_cylinder'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/cyl.png", 40, 40, "cylinder"))
        .appendField("Cylinder");
    this.appendValueInput("diameter1")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("diameter1");
    this.appendValueInput("diameter2")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("diameter2");
    this.appendValueInput("height")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("height");
    this.appendValueInput("sides")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("sides");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-cylinder');
  }
};

Blockly.Blocks['three_torus'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/torus.png", 40, 40, "torus"))
        .appendField("Torus");
    this.appendValueInput("diameter")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("diameter");
    this.appendValueInput("tube_diameter")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("tube diameter");
    this.appendValueInput("radial_segments")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("radial segments");
    this.appendValueInput("tubular_segments")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("tubular segments");
    this.appendValueInput("arc")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("arc angle");
    this.setOutput(true);
    this.setColour(120);
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-torus');

  }
};

Blockly.Blocks['three_text_function'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("function text3D");
    this.setColour(290);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-text3d');
  }
};

Blockly.Blocks['three_text'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/text.png", 40, 40, "text"))
        .appendField("3D Text");
    this.appendValueInput("text")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("text");
    this.appendValueInput("size")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("size");
    this.appendValueInput("height")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("height");
    this.setColour(120);
    this.setOutput(true);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-text3d');
  }
};

Blockly.Blocks['three_polygon'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/pol.png", 40, 40, "polygon"))
        .appendField("Polygon")
        .appendField(new Blockly.FieldTextInput("[[0,0],[10,0],[0,10]]"), "code");
    this.setOutput(true);
    this.setMutator(new Blockly.PolygonMutator([]));
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html#the-polygon');
  }
};

Blockly.Blocks['three_spiral'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/spiral.png", 40, 40, "polygon"))
        .appendField("Spiral");
    this.appendValueInput("height")
        .setCheck("Number")
        .appendField("height");
    this.appendValueInput("segments")
        .setCheck("Number")
        .appendField("segments");
//    this.appendValueInput("angle_z")
//        .setCheck(null)
//        .appendField("angle(z)")
//        .appendField(new Blockly.FieldTextInput("angle(z)"), "var");
    this.appendStatementInput("angle_p")
        .setCheck(null)
        .appendField("angle(p)");
    this.appendStatementInput("radius_p")
        .setCheck(null)
        .appendField("radius(p)");
//    this.appendValueInput("angle_p")
//        .appendField("angle(p)");
//    this.appendValueInput("radius_p")
//        .appendField("radius(p)");
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/shapes.html');
  }
};

Blockly.Blocks['three_extrude'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/extrude.png", 40, 40, "extrude"))
        .appendField("Extrude");
    this.appendValueInput("height")
        .setCheck("Number")
        .appendField("height");
    this.appendValueInput("segments")
        .setCheck("Number")
        .appendField("segments");
    this.appendValueInput("twist")
        .setCheck("Number")
        .appendField("twist angle");
    this.appendValueInput("polygon")
        .appendField("polygon");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#extrude');
  }
};

// TODO: JCOA Maybe use first letter lowercase for parameters and uppercase for block names.
Blockly.Blocks['three_lathe'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/lathe.png", 40, 40, "extrude"))
        .appendField("Lathe");
    this.appendValueInput("closed")
        .setCheck("Boolean")
        .appendField("closed");
    this.appendValueInput("divisions")
        .setCheck("Number")
        .appendField("divisions");
    this.appendValueInput("polygon")
        .appendField("polygon");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#lathe');
  }
};

Blockly.Blocks['three_extrude_path'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/path.png", 40, 40, "extrude"))
        .appendField("Extrude Path");
    this.appendValueInput("closed")
        .setCheck("Boolean")
        .appendField("closed");
    this.appendValueInput("divisions")
        .setCheck("Number")
        .appendField("divisions");
    this.appendValueInput("polygon")
        .appendField("polygon");
    this.appendValueInput("path")
        .appendField("path (polygon)");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#path');
  }
};

Blockly.Blocks['three_extrude_spiral'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/spiral.png", 40, 40, "extrude"))
        .appendField("Extrude Spiral");
    this.appendValueInput("height")
        .setCheck("Number")
        .appendField("height");
    this.appendValueInput("sections")
        .setCheck("Number")
        .appendField("segments");
    this.appendValueInput("polygon")
        .appendField("polygon");
    this.appendStatementInput("rotations_p")
        .setCheck(null)
        .appendField("rotations(p)");
    this.appendStatementInput("radius_p")
        .setCheck(null)
        .appendField("radius(p)");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#path');
  }
};

Blockly.Blocks['three_scale'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/scale.png", 40, 40, "scale"))
        .appendField("Scale");
    this.appendValueInput("coordinates")
        .appendField("[x,y,z]");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#scale');
  }
};

Blockly.Blocks['three_translate'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/translate.png", 40, 40, "translate"))
        .appendField("Translate");
    this.appendValueInput("coordinates")
        .appendField("[x,y,z]");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#translate');
  }
};

Blockly.Blocks['three_rotate'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/rotate.png", 40, 40, "rotate"))
        .appendField("Rotate");
    this.appendValueInput("coordinates")
        .appendField("[x,y,z]");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#rotate');
  }
};

Blockly.Blocks['three_subdivisions'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/subdivision.png", 40, 40, "setcolor"))
        .appendField("Subdivide");
    this.appendValueInput("subdivisions")
        .appendField("subdivisions");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#subdivision');
  }
};

Blockly.Blocks['three_setcolor'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/setcolor.png", 40, 40, "setcolor"))
        .appendField("Color");
    this.appendValueInput("color")
        .appendField("color");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/materials.html#set-color');
  }
};

Blockly.Blocks['three_settexture'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/texture.png", 40, 40, "setmaterial"))
        .appendField("Texture");
    this.appendValueInput("texture")
        .appendField("texture");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/materials.html#set-texture/');
  }
};

Blockly.Blocks['three_setmaterial'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/setcolor.png", 40, 40, "setmaterial"))
        .appendField("Material");
    this.appendValueInput("transparent")
        .setCheck('Boolean')
        .appendField("transparent");
    this.appendValueInput("opacity")
        .setCheck('Number')
        .appendField("opacity");
    this.appendValueInput("material")
        .appendField("material");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/materials.html#set-material');
  }
};

Blockly.Blocks['physi_creatematerial'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/physimaterial.png", 40, 40, "setmaterial"))
        .appendField("Add physics");
    this.appendValueInput("mass")
        .setCheck("Number")
        .appendField("mass");
    this.appendValueInput("friction")
        .setCheck("Number")
        .appendField("friction");
    this.appendValueInput("bounce")
        .setCheck("Number")
        .appendField("bounce");
    this.appendValueInput("object")
        .appendField("object");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/physics.html');
  }
};

Blockly.Blocks['three_basicmaterial'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/basicmat.png", 40, 40, "basicmaterial"))
        .appendField("Basic Material");
    this.appendValueInput("color")
        .appendField("color");
    this.appendValueInput("wireframe")
        .setCheck('Boolean')
        .appendField("wireframe");
    //this.appendValueInput("texture")
    //    .appendField("texture");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/materials.html#basic-material');
  }
};

Blockly.Blocks['three_lambertmaterial'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/setcolor.png", 40, 40, "basicmaterial"))
        .appendField("Lambert Material");
    this.appendValueInput("color")
        .appendField("color");
    this.appendValueInput("wireframe")
        .setCheck('Boolean')
        .appendField("wireframe");
    //this.appendValueInput("texture")
    //     .appendField("texture");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/materials.html#lambert-material');
  }
};

Blockly.Blocks['three_phongmaterial'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/phongmat.png", 40, 40, "basicmaterial"))
        .appendField("Phong Material");
    this.appendValueInput("color")
        .appendField("color");
    this.appendValueInput("wireframe")
        .setCheck('Boolean')
        .appendField("wireframe");
    //this.appendValueInput("texture")
    //    .appendField("texture");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/materials.html#phong-material');
  }
};

Blockly.Blocks['three_object_loader'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField("Load 3D object");
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("", 10, 10, "*")) // .appendField(new Blockly.FieldImage("", 64, 64, "*")) //https://www.gstatic.com/codesite/ph/images/star_on.gif
        .appendField(new Blockly.FieldLabel(""), "name");
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput(""), "id"); //.appendField(new Blockly.FieldLabel(""), "id");
    this.setOutput(true);
    this.setMutator(new Blockly.AssetObjectMutator([]));
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html#object-loader');
  }
};

Blockly.Blocks['three_image_loader'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField("Load texture");
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("", 64, 64, "*")) //https://www.gstatic.com/codesite/ph/images/star_on.gif
        .appendField(new Blockly.FieldLabel(""), "name");
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput(""), "id"); //.appendField(new Blockly.FieldLabel(""), "id");
    this.setOutput(true);
    this.setMutator(new Blockly.AssetImageMutator([]));
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html#image-loader');
  }
};

Blockly.Blocks['three_objloader'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/mesh.png", 40, 40, "basicmaterial"))
        .appendField("Load .obj");
    this.appendValueInput("url")
        .appendField("url");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
  }
};

Blockly.Blocks['three_stlloader'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/mesh.png", 40, 40, "basicmaterial"))
        .appendField("Load .stl");
    this.appendValueInput("url")
        .appendField("url");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
  }
};

Blockly.Blocks['three_parameter_float'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("float param");
    this.appendDummyInput()
        .appendField("name")
        .appendField(new Blockly.FieldTextInput(""), "name");
    this.appendDummyInput()
        .appendField("initial")
        .appendField(new Blockly.FieldTextInput("0"), "initial");
    this.appendDummyInput()
        .appendField("caption")
        .appendField(new Blockly.FieldTextInput(""), "caption");
    this.appendDummyInput()
        .appendField("min")
        .appendField(new Blockly.FieldTextInput("0"), "min");
    this.appendDummyInput()
        .appendField("max")
        .appendField(new Blockly.FieldTextInput("1"), "max");
    this.appendDummyInput()
        .appendField("step")
        .appendField(new Blockly.FieldTextInput("0"), "step");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['three_parameter_choice'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("choice param");
    this.appendDummyInput()
        .appendField("name")
        .appendField(new Blockly.FieldTextInput(""), "name");
    this.appendDummyInput()
        .appendField("initial")
        .appendField(new Blockly.FieldTextInput("0"), "initial");
    this.appendValueInput("values")
        .setCheck("Array")
        .appendField("values");
    this.appendValueInput("captions")
        .setCheck("Array")
        .appendField("captions");
    this.appendDummyInput()
        .appendField("caption")
        .appendField(new Blockly.FieldTextInput(""), "caption");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['three_parameter_value'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("params.")
        .appendField(new Blockly.FieldTextInput(""), "name");
    this.setInputsInline(false);
    this.setOutput(true);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/index.html');
  }
};

Blockly.Blocks['three_setshadow'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/setcolor.png", 40, 40, "setmaterial"))
        .appendField("SetShadow");
    this.appendValueInput("castshadow")
        .setCheck('Boolean')
        .appendField("castshadow");
    this.appendValueInput("receiveshadow")
        .setCheck('Boolean')
        .appendField("receiveshadow");
    this.appendValueInput("block")
        .appendField("block");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_shadowmapenabled'] = {
  init: function() {
    this.appendValueInput("shadowmapenabled")
        .setAlign(Blockly.ALIGN_RIGHT)
        .setCheck('Boolean')
        .appendField("shadowmapenabled");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_gridhelper'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/setcolor.png", 40, 40, "setmaterial"))
        .appendField("GridHelper");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
  }
};

Blockly.Blocks['three_axishelper'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/setcolor.png", 40, 40, "setmaterial"))
        .appendField("AxisHelper");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/scene.html');
  }
};

Blockly.Blocks['three_setlightshadow'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_LEFT)
        .appendField(new Blockly.FieldImage("images/setcolor.png", 40, 40, "setmaterial"))
        .appendField("SetLightShadow");
    this.appendValueInput("castshadow")
        .setCheck('Boolean')
        .appendField("castshadow");
    // this.appendValueInput("shadowdarkness") depreciated
    //     .appendField("shadowdarkness");     depreciated
    this.appendValueInput("shadowresolution")
        .appendField("shadowresolution");
    //this.appendValueInput("shadowmapheight")
        //.appendField("shadowmapheight");
    this.appendValueInput("shadowcamerafar")
        .appendField("shadowcamerafar");
    this.appendValueInput("isdirectional")
        .appendField("isdirectional");
    this.appendValueInput("shadowcamerasize")
        .appendField("shadowcamerasize");
    this.appendValueInput("camerahelper")
        .appendField("Camera helper");
    //this.appendValueInput("shadowcameraright")
        //.appendField("shadowcameraright");
    //this.appendValueInput("shadowcameratop")
        //.appendField("shadowcameratop");
    //this.appendValueInput("shadowcamerabottom")
        //.appendField("shadowcamebottom");
    this.appendValueInput("block")
        .appendField("block");
    this.setOutput(true);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/lights.html');
  }
};

Blockly.Blocks['three_clone'] = {
  init: function() {
    this.appendValueInput("object")
        .setCheck(null)
        .appendField(new Blockly.FieldImage("images/clone.png", 40, 40, "C"))
        .appendField("Clone");
    this.setOutput(true, null);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://makerscad-docs.readthedocs.io/en/latest/cad/transform.html#clone');
  }
};

Juan Carlos Orozco's notes:

See /deploy/Log_JCOA.txt first

*Installers available at https://gumroad.com/makerscad Support our proyect.

On this dist directory:
$ npm install

Edit the package name and version on package.json
Then pick one option and execute it:
node_modules/.bin/build --win32 --ia32
node_modules/.bin/build --win --x64
node_modules/.bin/build --linux --x64
node_modules/.bin/build --linux --ia32
node_modules/.bin/build --mac --x64

? Test mwl mac, windows, linux:
node_modules/.bin/build --mwl

(other option is to use npx for example: npx build --win --x64)

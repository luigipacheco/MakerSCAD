PK
     ��J�dv	   	      mmp{"ui":{}}PK
     ��JKe��5  5     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="bi_named_function" id="P9xfdB4p;g=99K`/!ck]" x="1" y="47"><field name="function_type">function </field><field name="name">print_points</field><field name="args">polygon</field><statement name="chain"><block type="mm_script_text" id="~agW1EAGzCWZJQ{TaxE["><field name="name">print_points.js</field></block></statement><next><block type="bi_named_function" id="gNo-;At31OE2*#2/E|UV"><field name="function_type">function </field><field name="name">z_fn</field><field name="args">shape, z_n, z_min, z_max</field><statement name="chain"><block type="mm_script_text" id="Wv%iyCA?oWMCF=y8EHAQ"><field name="name">z_fn.js</field></block></statement><next><block type="mm_println" id="ufulN}M^Z){)+#+HG3Py"><value name="msg"><shadow type="text" id="zuSOgU^mJ[g/HEeiOHxI"><field name="TEXT">abc</field></shadow><block type="bi_call_editable_return" id="+fe,%l6ek,z%lr?]vLLt"><mutation items="5"></mutation><field name="NAME">z_fn</field><value name="items1"><block type="three_polygon" id="!sF8`X,|[1%)V+_ID~9l"><field name="code">[[0,0],[10,5],[5,12],[-2,14],[6,19],[4,24],[-2,26],[-10,30],[0,33],[-15,36]]</field></block></value><value name="items2"><block type="math_number" id=".P?JL!5:2QpYSO51O9%#"><field name="NUM">200</field></block></value><value name="items3"><block type="math_number" id="cqo]*N21+uNCg2Xw?XGu"><field name="NUM">-10</field></block></value><value name="items4"><block type="math_number" id="6F@B!]Tu-E#.;SzpUeIJ"><field name="NUM">10</field></block></value></block></value></block></next></block></next></block></xml>PK
     ��JY6>�       jsfunction print_points(polygon){
  for(p of polygon){
      console1.log("("+p.x+","+p.y+"), ");
  }
}
function z_fn(shape, z_n, z_min, z_max){
  var polygon = shape.extractPoints().shape
  //var z_list = []
  var z_values = []

  console1.logln(polygon[polygon.length-1].y)

  if(polygon[polygon.length-1].y <= polygon[0].y){
      console.log("ERROR: Polygon sections must have positive slope (1)")
      return function(z){return 0}
  }

  var p_range = polygon[polygon.length-1].y-polygon[0].y
  var z_range = z_max-z_min
  // JCOA: Not sure if the scale is backwards or not needed
  var scale_factor = p_range/z_range
  var y_step = z_n/p_range

  var f_slope = function(i, pol){
      return (pol[i+1].y-pol[i].y)/(pol[i+1].x-pol[i].x)
  }

  // z_n
  var y = 0
  var p_i = 0
  var slope = f_slope(0, polygon)
  for(let i = 0; i < z_n; i++){
      y = y_step * i
      if(p_i < polygon.length-1 ){
          if(y >= polygon[p_i+1].y){
              p_i += 1
              slope = f_slope(p_i, polygon)
          }
          if(polygon[p_i+1].y <= polygon[p_i].y){
              console.log("ERROR: Polygon sections must have positive slope (2)")
              return function(z){return 0}
          }
          z_values.push([y, slope])
      }
  }

  // var first = true
  // var prev_y
  // var err = false
  // for(p of polygon){
  //     console1.log("("+p.x+","+p.y+"), ")
  //     z_list.push(p.y)
  //     if(first){
  //         first = false
  //     } else {
  //         if(p.y<= prev_y){
  //             err = true
  //             console1.logln("Error: Only positive slope between points is allowed!")
  //         }
  //     }
  //     prev_y = p.y
  // }

  // if(!err){
  //     if(z<=z_list[0]){
  //         ret_x = polygon[0].x
  //     } else {
  //         if(z>=z_list[z_list.length-1]){
  //             ret_x = polygon[z_list.length-1].x
  //         } else {
  //             let i = 0
  //             while(z > z_list[i]){
  //                 i += 1
  //             }
  //             if(z==z_list[i]){
  //                 ret_x = polygon[i].x
  //             } else {
  //               let p1 = polygon[i-1]
  //               let p2 = polygon[i]
  //               ret_x = p1.x+(z-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)
  //             }
  //         }
  //     }
  // }

  //return ret_x
  // z_min, z_max, z_n, z_range = z_max-z_min
  var z_step = z_range/z_n
  // z_values = [z, slope]
  // We will preload z_n values to this array

  // Convert z_min to polygon y_min and z_max to polygon y_max using scale and offset
  // Loop z_n values from z_min to z_max

  return function(z){
      // Test for z < z_min and z > z_max. Return z_values[0,0] and z_values[z_n-1, 0] respectively
      var v = (z - z_min)/z_step
      var i = Math.floor(v)
      var remainder = v - i
      //var quotient = Math.floor(y/x);
      //var remainder = y % x;
      return z_values[i,0] + remainder*z_values[i,1]
  }












}
console1.logln(z_fn(mm_polygon([[0,0],[10,5],[5,12],[-2,14],[6,19],[4,24],[-2,26],[-10,30],[0,33],[-15,36]]), 200, -10, 10));
PK
     ��J�����  �  	   functions{"print_points.js":{"hash":"3d_4af9daab61979b6862bf74fcc2a5630b","name":"print_points.js","data":"for(p of polygon){\n    console1.log(\"(\"+p.x+\",\"+p.y+\"), \");\n}","url":""},"z_fn.js":{"hash":"a19_62cf1f487b87737bb15394805bbf65ac","name":"z_fn.js","data":"var polygon = shape.extractPoints().shape\n//var z_list = []\nvar z_values = []\n\nconsole1.logln(polygon[polygon.length-1].y)\n\nif(polygon[polygon.length-1].y <= polygon[0].y){\n    console.log(\"ERROR: Polygon sections must have positive slope (1)\")\n    return function(z){return 0}\n}\n\nvar p_range = polygon[polygon.length-1].y-polygon[0].y\nvar z_range = z_max-z_min\n// JCOA: Not sure if the scale is backwards or not needed\nvar scale_factor = p_range/z_range\nvar y_step = z_n/p_range\n\nvar f_slope = function(i, pol){\n    return (pol[i+1].y-pol[i].y)/(pol[i+1].x-pol[i].x)\n}\n\n// z_n\nvar y = 0\nvar p_i = 0\nvar slope = f_slope(0, polygon)\nfor(let i = 0; i < z_n; i++){\n    y = y_step * i\n    if(p_i < polygon.length-1 ){\n        if(y >= polygon[p_i+1].y){\n            p_i += 1\n            slope = f_slope(p_i, polygon)\n        }\n        if(polygon[p_i+1].y <= polygon[p_i].y){\n            console.log(\"ERROR: Polygon sections must have positive slope (2)\")\n            return function(z){return 0}\n        }\n        z_values.push([y, slope])\n    }\n}\n\n// var first = true\n// var prev_y\n// var err = false\n// for(p of polygon){\n//     console1.log(\"(\"+p.x+\",\"+p.y+\"), \")\n//     z_list.push(p.y)\n//     if(first){\n//         first = false\n//     } else {\n//         if(p.y<= prev_y){\n//             err = true\n//             console1.logln(\"Error: Only positive slope between points is allowed!\")\n//         }\n//     }\n//     prev_y = p.y\n// }\n\n// if(!err){\n//     if(z<=z_list[0]){\n//         ret_x = polygon[0].x\n//     } else {\n//         if(z>=z_list[z_list.length-1]){\n//             ret_x = polygon[z_list.length-1].x\n//         } else {\n//             let i = 0\n//             while(z > z_list[i]){\n//                 i += 1\n//             }\n//             if(z==z_list[i]){\n//                 ret_x = polygon[i].x\n//             } else {\n//               let p1 = polygon[i-1]\n//               let p2 = polygon[i]\n//               ret_x = p1.x+(z-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)\n//             }\n//         }\n//     }\n// }\n\n//return ret_x\n// z_min, z_max, z_n, z_range = z_max-z_min\nvar z_step = z_range/z_n\n// z_values = [z, slope]\n// We will preload z_n values to this array\n\n// Convert z_min to polygon y_min and z_max to polygon y_max using scale and offset\n// Loop z_n values from z_min to z_max\n\nreturn function(z){\n    // Test for z < z_min and z > z_max. Return z_values[0,0] and z_values[z_n-1, 0] respectively\n    var v = (z - z_min)/z_step\n    var i = Math.floor(v)\n    var remainder = v - i\n    //var quotient = Math.floor(y/x);\n    //var remainder = y % x;\n    return z_values[i,0] + remainder*z_values[i,1]\n}\n\n\n\n\n\n\n\n\n\n\n\n","url":""}}PK
     ��J            	   textures/PK
     ��JC���         textures/metadata.json{}PK
     ��J               objects/PK
     ��JC���         objects/metadata.json{}PK 
     ��J�dv	   	                    mmpPK 
     ��JKe��5  5               *   xmlPK 
     ��JY6>�                 �  jsPK 
     ��J�����  �  	             �  functionsPK 
     ��J            	            �  textures/PK 
     ��JC���                   �  textures/metadata.jsonPK 
     ��J                        �  objects/PK 
     ��JC���                     objects/metadata.jsonPK      �  R    